import {BasicColumn} from '/@/components/Table';
import {FormSchema} from '/@/components/Table';
import {render} from '/@/utils/common/renderUtils';
//列表数据
export const columns: BasicColumn[] = [
  {
    title: '名称',
    align: "center",
    dataIndex: 'name'
  },
  {
    title: '单价',
    align: "center",
    dataIndex: 'money'
  },
  {
    title: '类型',
    align: "center",
    dataIndex: 'type_dictText'
  },
  {
    title: '图片',
    align: "center",
    dataIndex: 'img',
    customRender: render.renderImage,
  },
];
//查询数据
export const searchFormSchema: FormSchema[] = [];
//表单数据
export const formSchema: FormSchema[] = [
  {
    label: '名称',
    field: 'name',
    component: 'Input',
  },
  {
    label: '单价',
    field: 'money',
    component: 'InputNumber',
  },
  {
    label: '类型',
    field: 'type',
    component: 'JSelectMultiple',
    componentProps: {
      dictCode: "lx_shop_type"
    },
  },
  {
    label: '图片',
    field: 'img',
    component: 'JImageUpload',
    helpMessage: '最多上传3张图片',
    componentProps: {
      listType : 'picture',
      fileMax : 3,
    }
  },
  // TODO 主键隐藏字段，目前写死为ID
  {
    label: '',
    field: 'id',
    component: 'Input',
    show: false
  },
];
